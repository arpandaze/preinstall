timedatectl set-ntp true
timedatectl status

echo "--------------------------------------------------------"
echo "Partition:"
echo "Create partitions with cfdisk for swap, efi and system!"
echo "EFI    Size: 512M     Type: EFI System"
echo "Swap   Size: =RAM     Type: Linux swap"
echo "System Size: 15GB+    Type: Linux filesystem"
echo ""
echo "Notes:"
echo "Don't forget to set correct partition type!"
echo "Take note of partition paths (eg. sda1,sda2,sda3)"
echo "--------------------------------------------------------"
echo "Have you completed the above steps? (Y/N): "
read firstStep

if [[ $firstStep != "y" && $firstStep != "Y" ]];then
    exit
fi
clear

echo "Enter Partition Path! If you didn't note it in previous"
echo "step, exit and find it by entering fdisk --list"
echo "Partition Example: sda1, sdb2. Don't enter /dev/ part! "
echo "--------------------------------------------------------"
echo "Enter you EFI Partition: "
read efipart
echo "Enter you Swap Partition: "
read swappart
echo "Enter you EXT4 Linux Partition: "
read extpart

clear

mkfs.fat -F32 /dev/$efipart
mkswap /dev/$swappart
swapon /dev/$swappart
mkfs.ext4 /dev/$extpart

mount /dev/$extpart /mnt
pacstrap /mnt base linux linux-firmware
genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Asia/Kathmandu /etc/localtime
hwclock -systohc

pacman -S neovim
pacman -Syu

#GIT
pacman -S git
git config --global user.email "dazehere@yandex.com"
git config --global user.name "Daze"

#Backup LTS Kernel
pacman -s linux-lts

#Dependency Package
pacman -S base-devel

#XOrg Setup
pacman -S xorg xorg-server xorg-init xorg-xmessage

#Login Manager and Greeter Setup
pacman -S lightdm lightdm-webkit2-greeter
systemctl enable lightdm -f

#Window Manager Setup
pacman -S xmonad
pacman -s xmonad-contrib
pacman -S xmobar
pacman -S nitrogen
pacman -S picom
pacman -S trayer
pacman -S nm-applet

#Terminal Emulator
pacman -S alacritty

#Alternative Terminal Emulator
pacman -s xterm

#Text Editor
pacman -S neovim

#Python and Pip
pacman -S python
pacman -S python-pip

#Fuzzy Finder
pacman -S fzf

#Neofetch
pacman -S neofetch

#Zsh shell and related plugins
pacman -S zsh
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

#Packages for sound
pacman -S pavucontrol
pacman -S pulseaudio

#Screenshot tool
pacman -S maim

#Web Browser
pacman -S firefox

#NodeJS and NPM
pacman -S nodejs
pacman -S npm

#NVIM Setup and Configs
npm install -g neovim
pip install neovim
mkdir -p ~/.config/nvim
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

#Utilities Installation
pacman -S zip
pacman -S cabal-install
pacman -S pacman-contrib
pacman -S pacman-contrib
pacman -S powerline-fonts
pacman -S xclip
pacman -S flatpak
pacman -S ripgrep
pacman -S ffmpeg
pacman -S youtube-dl
pacman -S slock
pacman -S vlc
pacman -S xdotool
pacman -S rhythmbox
pacman -S ttf-opensans
pacman -S rofi
pacman -S the_silver_searcher
pacman -S clang
pacman -S gmrun
pacman -S caps
pacman -S alsa-utils
pacman -S alsa-firmware
