ln -sf /usr/share/zoneinfo/Asia/Kathmandu /etc/localtime
hwclock -systohc

pacman -S neovim
pacman -Syu

#GIT
pacman -S git
git config --global user.email "dazehere@yandex.com"
git config --global user.name "Daze"

#Backup LTS Kernel
pacman -s linux-lts

#Dependency Package
pacman -S base-devel

#XOrg Setup
pacman -S xorg xorg-server xorg-init xorg-xmessage

#Login Manager and Greeter Setup
pacman -S lightdm lightdm-webkit2-greeter
systemctl enable lightdm -f

#Window Manager Setup
pacman -S xmonad
pacman -s xmonad-contrib
pacman -S xmobar
pacman -S nitrogen
pacman -S picom
pacman -S trayer
pacman -S nm-applet

#Terminal Emulator
pacman -S alacritty

#Alternative Terminal Emulator
pacman -s xterm

#Text Editor
pacman -S neovim

#Python and Pip
pacman -S python
pacman -S python-pip

#Fuzzy Finder
pacman -S fzf

#Neofetch
pacman -S neofetch

#Zsh shell and related plugins
pacman -S zsh
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

#Packages for sound
pacman -S pavucontrol
pacman -S pulseaudio

#Screenshot tool
pacman -S maim

#Web Browser
pacman -S firefox

#NodeJS and NPM
pacman -S nodejs
pacman -S npm

#NVIM Setup and Configs
npm install -g neovim
pip install neovim
mkdir -p ~/.config/nvim
curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

#Utilities Installation
pacman -S zip
pacman -S cabal-install
pacman -S pacman-contrib
pacman -S pacman-contrib
pacman -S powerline-fonts
pacman -S xclip
pacman -S flatpak
pacman -S ripgrep
pacman -S ffmpeg
pacman -S youtube-dl
pacman -S slock
pacman -S vlc
pacman -S xdotool
pacman -S rhythmbox
pacman -S ttf-opensans
pacman -S rofi
pacman -S the_silver_searcher
pacman -S clang
pacman -S gmrun
pacman -S caps
pacman -S alsa-utils
pacman -S alsa-firmware
